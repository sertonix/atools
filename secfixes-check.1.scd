secfixes-check(1)

# NAME

secfixes-check - A linter for Alpine Linux's secfixes declarations in APKBUILDs

# SYNOPSIS

*secfixes-check* <apkbuild...>

# DESCRIPTION

secfixes-check is a linter for secfixes declarations inside APKBUILDs. It
reads the file given in each command-line argument and checks for policy
violations for secfixes and also validate the yaml by trying to load it
with the 'lyaml' module from lua. See *alint(5)* under the secfixes-check
section for an explanation of each violation that secfixes-checks for.

secfixes-check will print the policy violations, and exit with the code
that correspond to the number of violations found, if no violations are
found nothing is printed and secfixes-check will exit with code 0.

# OUTPUT

secfixes-check will print to stdout whenever a policy violation is found in the
following format

	SEVERITYCERTAINITY:[TAG]:PATH::MSG

- *SEVERITY* refers to how severe a violation is, ranging from *S* to *M*.
- *CERTAINITY* refers to how likely it is not a false positive, ranging from *C* to *P*
- *TAG* refers to the tag of the violation, see *alint(5)* for more details.
- *PATH* refers to the path given for *apkbuild-lint* to check.
- *MSG* is a short message meant for humans to know what is the violation.

# AUTHORS

Maintained by Leo <thinkabit.ukim@gmail.com>

# SEE ALSO

*alint(5)* *apkbuild-lint(1)*
