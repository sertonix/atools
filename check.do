exec >&2
printf -- '\033[0m\033[32m>>>\033[0m Testing apkbuild-lint\n'
bats --tap tests/apkbuild-lint.bats

printf -- '\033[0m\033[32m>>>\033[0m Testing initd-lint\n'
bats --tap tests/initd-lint.bats

printf -- '\033[0m\033[32m>>>\033[0m Testing secfixes-check\n'
bats --tap tests/secfixes-check.bats

printf -- '\033[0m\033[32m>>>\033[0m Testing provided-bad-version-regex\n'
bats --tap tests/bad-version-regex.bats
